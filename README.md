# Slackstuff

My scripts, programs, etc for Slackware Linux. I try to keep dependencies to a
minimun and the goal is to have a collection of useful scripts that just run on
a full vanilla Slackware installation, including the last stable release and
current.

## Scripts

- *slkernel* A kernel management utility